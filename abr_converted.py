import os, subprocess
import sys
import traceback
import uuid
QUALITIES = [
    "360x640", 
    "540x960"
    ]
path = "/Users/tuankq/Downloads/reimi_loop_short"

def make_hls_final(
        quality,
        input_video_path,
        output_hls_dir,
        segment_time,  # in seconds
        m3u8_name="playlist.m3u8",
):
    if not os.path.exists(output_hls_dir):
        os.makedirs(output_hls_dir)
    input_video_path = input_video_path
    cmd = f"""ffmpeg -i {input_video_path} -s {quality} -c:v libx264
            -force_key_frames "expr:gte(t,n_forced*{segment_time})" 
            -start_number 0 -hls_time {segment_time} 
            -hls_list_size 0 -r 25
            -hls_segment_filename "{output_hls_dir}/playlist%03d.ts" -f hls "{output_hls_dir + "/" + m3u8_name}"
    """.replace("\n"," ")
    while "  " in cmd:
        cmd = cmd.replace("  ", " ")
    return cmd
    # subprocess.run(cmd, capture_output=True, text=True)

def add_empty_audio(path):
    for i in os.listdir(path):
        print(f"ffmpeg -i {path}/{i} -f lavfi -i anullsrc -c:v copy -c:a aac -b:a 16k -shortest ./original_videos/{i}")
    

def main():
    # Check if there are any command-line arguments
    if len(sys.argv) < 2:
        print("Please set input folder}")
        return
    folder = sys.argv[1]
    output = sys.argv[2]
    files = os.listdir(folder)
    cmds = []
    for file in files:
        source_output_file_name = f"{folder}/{file}".replace("//", "/")
        video_code = file.split(".")[0]
        try:
            for i in QUALITIES:
                cmd = make_hls_final(i, source_output_file_name, f"./output/{output}/{video_code}/{i}", segment_time=0.5)
                cmds.append(cmd)
        except:
            print(f"Exception: {traceback.format_exc()}")
    with open("./run.sh", "w") as exf:
        exf.write("\n".join(cmds))



if __name__ == "__main__":
    main()