import os, subprocess
import traceback
import uuid
QUALITIES = [
    # "180x320", 
    # "270x480", 
    "360x640", 
    "540x960"
    ]
BITRATES = {
    "540x960": {
        "max": "750k",
        "buffer": "1500k"
    },
    "360x640": "500k",
    "270x480": {
        "max": "300k",
        "buffer": "600k"
    }
}
def make_hls_final(
        quality,
        input_video_path,
        output_hls_dir,
        segment_time,  # in seconds
        # resolution,
        m3u8_name="playlist.m3u8",
        profile="high10",  # profile must be high10 for 10-bit video
):
    if not os.path.exists(output_hls_dir):
        os.makedirs(output_hls_dir)
    input_video_path = input_video_path
    cmd = f"""ffmpeg -i {input_video_path} -s {quality} -c:v libx264
    -force_key_frames "expr:gte(t,n_forced*{segment_time})" 
    -start_number 0 -hls_time {segment_time} 
    -hls_list_size 0 -r 25
    -hls_segment_filename "{output_hls_dir}/playlist%03d.ts" -f hls "{output_hls_dir + "/" + m3u8_name}"
    """.replace("\n"," ")
    while "  " in cmd:
        cmd = cmd.replace("  ", " ")
    return cmd
    # subprocess.run(cmd, capture_output=True, text=True)

if __name__ == "__main__":
    folder = "/Users/tuankq/workspace/demo/adaptive_bitrate_hls/lipsynced_videos"
    files = os.listdir(folder)
    cmds = []
    for file in files:
        source_output_file_name = f"{folder}/{file}"
        video_code = file.split(".")[0]
        try:
            for i in QUALITIES:
                cmd = make_hls_final(i, source_output_file_name, f"./edited_converted/{video_code}/{i}", segment_time=0.5)
                cmds.append(cmd)
        except:
            print(f"Exception: {traceback.format_exc()}")
    with open("./run.sh", "w") as exf:
        exf.write("\n".join(cmds))