
import subprocess
import os
import http.server
import socketserver

# Define the target resolutions heights
# The width will be set to -2 to automatically adjust while maintaining the aspect ratio
resolutions = {
    "240p": "-2:240",
    "360p": "-2:360",
    "720p": "-2:720"
}

# Input video file
source_videos_path = "./FortuneTeller_Female_Day"

def transcode_and_segment():
    source_videos = os.listdir(source_videos_path)
    source_videos = [i for i in source_videos if ".ts" in i]
    for source_video in source_videos:
        for resolution in resolutions.items():
            out_put_path = f"{source_videos_path}/{resolution[0]}"
            os.makedirs(out_put_path, exist_ok=True)
            command = [
                'ffmpeg',
                '-i', f"{source_videos_path}/{source_video}",
                '-vf', f'scale={resolution[1]}',  # Scale video while maintaining aspect ratio
                '-c:v', 'libx264',
                '-strict', 'experimental',
                '-c:a', 'aac',
                f"{out_put_path}/{source_video}"
            ]
            print(command)
            subprocess.run(command)

def start_server():
    PORT = 8000
    Handler = http.server.SimpleHTTPRequestHandler

    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print(f"Serving at port {PORT}")
        httpd.serve_forever()

if __name__ == "__main__":
    transcode_and_segment()
    start_server()
