import os, subprocess
import traceback
import uuid
QUALITIES = [
    # "180x320", 
    "270x480", 
    # "360x640", 
    "540x960"
    ]
BITRATES = {
    "540x960": {
        "max": "750k",
        "buffer": "1500k"
    },
    "360x640": "500k",
    "270x480": {
        "max": "300k",
        "buffer": "600k"
    }
}

def split_video(input_file: str, m3u8_folder_path: str,
                segment_duration=0.5, quality=None):
    ts_folder_path = m3u8_folder_path
    if not os.path.exists(ts_folder_path):
        os.makedirs(ts_folder_path, exist_ok=True)
    print(f"Splitting video {input_file} to HLS format")
    print(f"Output m3u8 folder: {m3u8_folder_path}")
    print(f"Output ts folder: {ts_folder_path}")
    ffmpeg_command = [
            "ffmpeg",
            # "-t", "2",
            "-i",
            input_file,
            "-s",
            quality,
            "-c:v",
            "copy",
            # "-c:v","libx264",
            "-f",
            "mp4",
            "-preset",
            "ultrafast",
            # "-profile:v", "high10",
            # "-level", "3.0",
            "-force_key_frames",
            f'"expr:gte(t,n_forced*{segment_duration})"',  # "0.5",
            "-start_number",
            "0",
            "-f",
            "hls",
            "-hls_time",
            str(segment_duration),
            "-hls_list_size",
            "0",
            "-g",
            "25",  # Set the output frame rate to 25 fps
            "-r",
            "25",  # Set the output frame rate to 25 fps
            # "-loglevel", "error",
            "-hide_banner",
            "-hls_segment_filename",
            ts_folder_path + "/playlist%03d.ts",
            m3u8_folder_path + "/" + "playlist.m3u8",
        ]
    print(" ".join(ffmpeg_command))
    return " ".join(ffmpeg_command)
    # try:
    #     subprocess.check_output(ffmpeg_command, stderr=subprocess.STDOUT)
    # except subprocess.CalledProcessError as e:
    #     print](f"Error splitting video: {e.output}")
    #     raise e
    # print("Finished splitting video")


if __name__ == "__main__":
    folder = "/Users/tuankq/Downloads/process_20240226"
    files = os.listdir(folder)
    cmds = []
    for file in files:
        source_output_file_name = f"{folder}/{file}"
        video_code = file.split(".")[0]
        try:
            for i in QUALITIES:
                cmd = split_video(source_output_file_name, f"./edited_converted/{video_code}/{i}", segment_duration=0.5, quality=i)
                cmds.append(cmd)
        except:
            print(f"Exception: {traceback.format_exc()}")
    with open("./run.sh", "w") as exf:
        exf.write("\n".join(cmds))