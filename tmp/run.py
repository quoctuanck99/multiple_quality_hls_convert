import os, subprocess
import traceback
import uuid
SOURCE_PATH = "/data/source"
EDITED_PATH = "/data/input-videos/edited"
QUALITIES = ["180x320", "270x480", "360x640", "540x960"]

def make_hls_final(
        quality,
        input_video_path,
        output_hls_dir,
        segment_time,  # in seconds
        # resolution,
        m3u8_name="playlist.m3u8",
        profile="high10",  # profile must be high10 for 10-bit video
):
    if not os.path.exists(output_hls_dir):
        os.makedirs(output_hls_dir)
    input_video_path = input_video_path

    cmd = [
        "ffmpeg",
        "-i", input_video_path,
        # "-profile:v",profile,
        # "-level",
        # "3.0",
        "-s", quality,
        "-force_key_frames",
        f"expr:gte(t,n_forced*{segment_time})",
        "-start_number", "0",
        "-hls_time", str(segment_time),
        "-hls_list_size", "0",
        "-r", "25",  # Set the output frame rate to 25 fps
        "-hls_segment_filename", output_hls_dir + "/playlist%03d.ts",
        "-f", "hls",
        output_hls_dir + "/" + m3u8_name,
    ]
    print(" ".join(cmd))
    subprocess.run(cmd, capture_output=True, text=True)



def concat_video(path, output_name):
    file_list = uuid.uuid4()
    # Command to generate file list
    generate_file_list_cmd = f"for f in {path}/*.ts; do echo \"file '$f'\" >> {file_list}.txt; done"
    print(generate_file_list_cmd)
    # Command to concatenate files using ffmpeg
    concatenate_cmd = f"ffmpeg -f concat -safe 0 -i {file_list}.txt -c copy ./{output_name}"
    print(concatenate_cmd)
    # Run the commands in the shell
    try:
        # Execute the first command to generate the file list
        subprocess.run(generate_file_list_cmd, shell=True, check=True)
        # Execute the second command to concatenate the .ts files into a single .mp4
        subprocess.run(concatenate_cmd, shell=True, check=True)
        print("Commands executed successfully.")
        return True
    except subprocess.CalledProcessError as e:
        print(f"An error occurred: {e}")
        return False


if __name__ == "__main__":
    video_code = "B005C004_231215BU"
    source = f"{SOURCE_PATH}/{video_code}"
    edited = f"{EDITED_PATH}/{video_code}"
    source_output_file_name = f"{video_code}_source.mp4"
    edited_output_file_name = f"{video_code}_edited.mp4"
    try:
        # concat source:
        print("concat source video")
        concat_video(source, source_output_file_name)
        print("concat edited video")
        concat_video(edited, edited_output_file_name)
        for i in QUALITIES:
            make_hls_final(i, source_output_file_name, f"./converted/{video_code}/source/{i}", segment_time=0.25)
            make_hls_final(i, edited_output_file_name, f"./converted/{video_code}/edited/{i}", segment_time=0.25)
    except:
        print(f"Exception: {traceback.format_exc()}")

    # concat edited:
    # for i in qualities:
    #     make_hls_final(i, f"/Users/tuankq/Desktop/Test/edited.mp4", f"./converted/edited/{i}", segment_time=0.5)