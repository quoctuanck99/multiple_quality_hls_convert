import os, subprocess
import sys
import traceback
import uuid
import logging

# Configure logging
logging.basicConfig(filename='logs.log', level=logging.DEBUG, filemode='w',
                    format='%(asctime)s - %(levelname)s - %(message)s')

SOURCE_PATH = "/data/source"
EDITED_PATH = "/data/input-videos/edited"
QUALITIES = ["180x320", "270x480", "360x640", "540x960"]
TMP_DIR = "./tmp"
OUTPUT_DIR = "./converted"

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

if not os.path.exists(TMP_DIR):
    os.makedirs(TMP_DIR)

def make_hls_final(
        quality,
        input_video_path,
        output_hls_dir,
        segment_time,  # in seconds
        # resolution,
        m3u8_name="playlist.m3u8",
        profile="high10",  # profile must be high10 for 10-bit video
):
    if not os.path.exists(output_hls_dir):
        os.makedirs(output_hls_dir)
    input_video_path = input_video_path

    cmd = [
        "ffmpeg",
        "-i", input_video_path,
        # "-profile:v",profile,
        # "-level",
        # "3.0",
        "-s", quality,
        "-force_key_frames",
        f"expr:gte(t,n_forced*{segment_time})",
        "-start_number", "0",
        "-hls_time", str(segment_time),
        "-hls_list_size", "0",
        "-r", "25",  # Set the output frame rate to 25 fps
        "-hls_segment_filename", output_hls_dir + "/playlist%03d.ts",
        "-f", "hls",
        output_hls_dir + "/" + m3u8_name,
    ]
    logging.info(" ".join(cmd))
    subprocess.run(cmd, capture_output=True, text=True)



def concat_video(path, output_name):
    file_list = f"{TMP_DIR}/{uuid.uuid4()}"
    # Command to generate file list
    generate_file_list_cmd = f"for f in {path}/*.ts; do echo \"file '$f'\" >> {file_list}.txt; done"
    logging.info(generate_file_list_cmd)
    # Command to concatenate files using ffmpeg
    concatenate_cmd = f"ffmpeg -f concat -safe 0 -i {file_list}.txt -c copy {output_name} -y"
    logging.info(concatenate_cmd)
    # Run the commands in the shell
    try:
        # Execute the first command to generate the file list
        subprocess.run(generate_file_list_cmd, shell=True, check=True)
        # Execute the second command to concatenate the .ts files into a single .mp4
        subprocess.run(concatenate_cmd, shell=True, check=True)
        logging.info("Commands executed successfully.")
        return True
    except subprocess.CalledProcessError as e:
        logging.info(f"An error occurred: {e}")
        return False
    # finally:
    #     subprocess.run(f"rm -f {file_list}")


if __name__ == "__main__":
    # video_code = sys.argv[1]
    source_list = os.listdir(SOURCE_PATH)
    edited_list = os.listdir(EDITED_PATH)
    for video_code in source_list:
        if video_code in edited_list:
            source = f"{SOURCE_PATH}/{video_code}"
            edited = f"{EDITED_PATH}/{video_code}"
            source_output_file_name = f"{video_code}_source.mp4"
            edited_output_file_name = f"{video_code}_edited.mp4"
            try:
                # concat source:
                logging.info("concat source video")
                source_output_path = f"{TMP_DIR}/{edited_output_file_name}"
                concat_video(source, source_output_path)
                logging.info("concat edited video")
                edited_output_path = f"{TMP_DIR}/{edited_output_file_name}"
                concat_video(edited, edited_output_path)
                logging.info("generate variants")
                for i in QUALITIES:
                    logging.info(f"create sources for {i}")
                    make_hls_final(i, source_output_path, f"{OUTPUT_DIR}/{video_code}/source/{i}", segment_time=0.5)
                    logging.info(f"create edited for {i}")
                    make_hls_final(i, edited_output_path, f"{OUTPUT_DIR}/{video_code}/edited/{i}", segment_time=0.5)
            except:
                logging.info(f"{video_code} Exception: {traceback.format_exc()}")
        else:
            logging.info(f"{video_code} in edited_list: {video_code in edited_list}")
            logging.info(f"{video_code} in source_list: {video_code in source_list}")