import os, subprocess
import sys
import traceback

def add_empty_audio(path):
    cmds = []
    for i in os.listdir(path):
        cmds.append(f"ffmpeg -i {path}/{i} -f lavfi -i anullsrc -c:v copy -c:a aac -b:a 16k -shortest ./original_videos/{i}")
    with open("./add_empty_audio.sh", "w") as exf:
        exf.write("\n".join(cmds))

def main():
    # Check if there are any command-line arguments
    if len(sys.argv) < 1:
        print("Please set input folder}")
        return
    input = sys.argv[1]
    add_empty_audio(input)

if __name__ == "__main__":
    main()