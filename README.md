Run this to make video muted
```sh
python3 add_empty_audio.py <videos_input_folder_path>
```
Then run  
```
chmod +x ./add_empty_audio.sh
./add_empty_audio.sh
```
Run this to convert video to abr variants:
```sh
python3 abr_converted.py <videos_input_folder_path> <output_name>
```
Then run  
```
chmod +x ./run.sh
./run.sh
```